# Awal

Welcome to the Awal project! This Angular application was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.9.

## Project Overview

Awal is a web application that integrates 3D shapes using Angular and Three.js. It provides functionalities to add, manipulate, and visualize geometries in a 3D environment. The project also includes a user interface for managing geometries and a responsive design to adapt to different screen sizes.

## Features

- **3D Geometry Management**: Add, manipulate, and visualize geometries such as boxes, spheres, cylinders, and toruses.
- **Responsive Design**: The application is designed to adapt to various screen sizes, including mobile and desktop views.
- **Interactive UI**: Includes forms for adding geometries, color pickers, and interactive controls for manipulating geometries.
- **Dynamic Geometry List**: Manage a list of added geometries with options to modify their positions and colors.

## User Interface Components

- **Three Container**: Displays the 3D scene where geometries are rendered.
- **Geometry List**: Lists all added geometries with controls for modifying their properties and deleting them.
- **Geometry Form**: Provides a form to add new geometries, including options for selecting geometry types, colors, and positions.

## Responsive Design

The application is designed to be responsive and adapts to various screen sizes. Key responsive features include:

- **Mobile Mode**: On screens smaller than a tablet, the geometry list can be toggled with a button to ensure a clean and usable interface.
- **Dropdown Toolbar**: The toolbar with controls for adding and managing geometries is positioned in the bottom right corner of the three-container map for easy access.

## Technologies Used

- **Angular**: A TypeScript-based web application framework. This project uses Angular version 11.2.7.
- **Three.js**: A JavaScript library for 3D graphics. This project uses Three.js version 0.126.1.
- **Bootstrap**: A CSS framework for responsive design. This project uses Bootstrap version 4.6.0.
- **RxJS**: A library for reactive programming using Observables. This project uses RxJS version 6.6.0.
- **TypeScript**: A superset of JavaScript that compiles to plain JavaScript. This project uses TypeScript version 4.1.6.

## Getting Started

### Development Server

To start the development server, run:
```bash
ng serve
```
Open your browser and navigate to http://localhost:4200/. The application will automatically reload if you make changes to any of the source files.

## Code Scaffolding
You can generate new components, directives, pipes, services, and more using the Angular CLI commands:

```
ng generate component component-name
ng generate directive|pipe|service|class|guard|interface|enum|module
```

Replace component-name with the name of the component you wish to create.

## Build

Run ng build to build the project. The build artifacts will be stored in the dist/ directory.

## Running unit tests

Run ng test to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run ng e2e to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Contributing
If you would like to contribute to the Awal project, please follow the standard Git workflow:

1. Fork the repository.
2. Create a feature branch.
3. Commit your changes.
4. Push the branch to your fork.
5. Open a pull request to the main repository.

Your contributions are welcome and appreciated!

## Further help

To get more help on the Angular CLI use ng help or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.