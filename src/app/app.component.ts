import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit{
  title = 'Awal';

  isLoading: boolean = true;
  contentClass: string = 'hidden-content';

  ngOnInit() {
    setTimeout(() => {
      this.isLoading = false;
      this.contentClass = 'show-content';
    }, 3000); // 3 seconds
  }
}
