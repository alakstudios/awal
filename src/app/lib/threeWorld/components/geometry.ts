import {
  BoxGeometry,
  BufferGeometry,
  CylinderGeometry,
  MathUtils,
  Mesh,
  MeshStandardMaterial,
  SphereGeometry,
  TextureLoader,
  TorusGeometry,
} from 'three';

function createMaterial(color: any) {
  return new MeshStandardMaterial({ color });
}

export function createGeometry(geometryType: string, color: any) {
  let geometry: BufferGeometry;
  
  switch (geometryType) {
    case 'BoxGeometry':
      geometry = new BoxGeometry(1, 1, 1);
      break;
    case 'SphereGeometry':
      geometry = new SphereGeometry(0.5, 10, 10);
      break;
    case 'CylinderGeometry':
      geometry = new CylinderGeometry(0.5, 0.5, 1, 10, 10);
      break;
    case 'TorusGeometry':
      geometry = new TorusGeometry(1, 0.5, 10, 10);
      break;
    default:
      throw new Error(`Unknown geometry type: ${geometryType}`);
  }

  const material = createMaterial(color);
  return new Mesh(geometry, material);
}