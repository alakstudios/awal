import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { World } from '../lib/threeWorld/World';
import { Geometry } from '../model/geometry';

@Injectable({
  providedIn: 'root'
})
export class ThreeWorldService {
  private world: World;
  geometries: Geometry[] = [];

  newGeometries = new Subject<Geometry[]>();
  
  constructor() {
    this.world = new World();
  }
  
  createWorld(container: HTMLElement){
    this.world.bind(container);
    this.world.start();
  }
  
  addGeometry(geometry: Geometry) {
    const geo = this.world.addGeometry(geometry);
    let geom = new Geometry(geo.id, geo.position, geo.geometry.type, geo.material.color);
    this.geometries.push(geom);
    this.newGeometries.next(this.geometries);
  }
  
  deleteGeometry(id: number) {
    this.world.deleteGeometry(id);
    let index = this.geometries.findIndex(geo => geo.id == id);
    if (index !== -1) {
      this.geometries.splice(index, 1);
      this.newGeometries.next(this.geometries);
    }
  }

  changeTransformMode(mode: string | null) {
    if (this.world) {
      if (mode) {
        this.world.setTransformMode(mode);
        console.log(`Transform mode set to: ${mode}`);
      } else {
        this.world.setTransformMode(null);
        this.toggleTransform(false);
        console.log('Transform mode deselected');
      }
    }
  }

  toggleTransform(enabled: boolean) {
    if (this.world) {
      this.world.toggleTransform(enabled);
      console.log(`Transformations ${enabled ? 'enabled' : 'disabled'}`);
    }
  }
}
