import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  ThreeWorldService
} from 'src/app/service/three-world.service';

@Component({
  selector: 'app-geometry-form',
  templateUrl: './geometry-form.component.html',
  styleUrls: ['./geometry-form.component.css']
})
export class GeometryFormComponent implements OnInit {
  geometryForm: FormGroup;
  formVisible = false;

  // Geometry types for button group
  geometryModes = [{
      label: 'Box',
      value: 'BoxGeometry',
      icon: '<img width="24" height="24" src="https://img.icons8.com/external-those-icons-lineal-those-icons/24/FFFFFF/external-Cube-design-those-icons-lineal-those-icons.png" alt="external-Cube-design-those-icons-lineal-those-icons"/>'
    },
    {
      label: 'Sphere',
      value: 'SphereGeometry',
      icon: '<img width="24" height="24" src="https://img.icons8.com/external-those-icons-lineal-those-icons/24/000000/external-Sphere-design-those-icons-lineal-those-icons.png" alt="external-Sphere-design-those-icons-lineal-those-icons"/>'
    },
    {
      label: 'Cylinder',
      value: 'CylinderGeometry',
      icon: '<img width="24" height="24" src="https://img.icons8.com/external-those-icons-lineal-those-icons/24/000000/external-Cylinder-design-those-icons-lineal-those-icons.png" alt="external-Cylinder-design-those-icons-lineal-those-icons"/>'
    },
    {
      label: 'Torus',
      value: 'TorusGeometry',
      icon: '<img width="24" height="24" src="https://img.icons8.com/external-those-icons-lineal-those-icons/24/external-donut-chart-charts-infographic-those-icons-lineal-those-icons-6.png" alt="external-donut-chart-charts-infographic-those-icons-lineal-those-icons-6"/>'
    }
  ];

  constructor(private threeService: ThreeWorldService) {}

  ngOnInit(): void {
    this.geometryForm = new FormGroup({
      position: new FormGroup({
        x: new FormControl(0, Validators.required),
        y: new FormControl(0, Validators.required),
        z: new FormControl(0, Validators.required),
      }),
      color: new FormControl('#ffffff', Validators.required),
      name: new FormControl(null, Validators.required)
    });
  }
  
  selectGeometry(value: string) {
    this.geometryForm.controls.name.setValue(value);
  }

  addGeometry() {
    if (this.geometryForm.valid) {
      this.threeService.addGeometry(this.geometryForm.value);
      this.toggleForm(); // Close form after adding
    }
  }

  toggleForm() {
    this.formVisible = !this.formVisible;
  }
}
