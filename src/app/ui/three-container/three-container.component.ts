import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ThreeWorldService } from 'src/app/service/three-world.service';

@Component({
  selector: 'app-three-container',
  templateUrl: './three-container.component.html',
  styleUrls: ['./three-container.component.css']
})
export class ThreeContainerComponent implements AfterViewInit {
  @ViewChild('threeContainer') threeContainer!: ElementRef;

  transformModes: Array<{ label: string, value: string, icon: SafeHtml }> = [];
  toolbarVisible = false; // Track toolbar visibility
  enabled = false;
  selectedMode: string | null = null;

  constructor(private threeService: ThreeWorldService, private sanitizer: DomSanitizer) { }

  ngAfterViewInit(): void {
    const container = this.threeContainer.nativeElement;
    this.threeService.createWorld(container);

    // Initialize transform modes here
    this.transformModes = [
      { label: 'Move', value: 'translate', icon: this.sanitizer.bypassSecurityTrustHtml('<img width="20" height="20" src="https://img.icons8.com/ios-glyphs/30/FFFFFF/move.png" alt="move"/>') },
      { label: 'Rotate', value: 'rotate', icon: this.sanitizer.bypassSecurityTrustHtml('<img width="20" height="20" src="https://img.icons8.com/ios-glyphs/30/FFFFFF/rotate.png" alt="rotate"/>') },
      { label: 'Scale', value: 'scale', icon: this.sanitizer.bypassSecurityTrustHtml('<img width="20" height="20" src="https://img.icons8.com/ios-glyphs/30/FFFFFF/resize-diagonal.png" alt="scale"/>') }
    ];
  }

  selectTransformMode(mode: string) {
    // Toggle the transform mode
    if (this.selectedMode === mode) {
      // If the same mode is clicked again, deselect it
      this.selectedMode = null;
      this.toggleTransform();
    } else {
      // Select the new mode
      this.selectedMode = mode;
      this.threeService.changeTransformMode(mode);
      if (!this.enabled) {
        this.toggleTransform();
      }
    }
  }

  toggleTransform() {
    this.enabled = !this.enabled;
    this.threeService.toggleTransform(this.enabled);
  }

  toggleToolbar() {
    this.toolbarVisible = !this.toolbarVisible;
  }
}
